var moduleApp = angular.module('app',[
    //npm
    'ngAnimate',
    'ui.router',
    //custom
    'app.view1',
    'app.view2',
    'app.view3',
    'app.view4',
    'app.services',
    'app.directives'
]);

moduleApp.config(function($stateProvider, $urlRouterProvider){
    $stateProvider.state('view1',{
        url:'/view1',
        templateUrl:'views/view1/view1.html',
        controller:'view1Ctrl'
    });
    $stateProvider.state('view2',{
        url:'/view2',
        templateUrl:'views/view2/view2.html',
        controller:'view2Ctrl'
    });
    $stateProvider.state('view3',{
        url:'/view3',
        templateUrl:'views/view3/view3.html',
        controller:'view3Ctrl'
    });
    $stateProvider.state('view4',{
        url:'/view4',
        templateUrl:'views/view4/view4.html',
        controller:'view4Ctrl'
    });
    $urlRouterProvider.otherwise('/view1');
});
