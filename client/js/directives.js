angular.module('app.directives',[])

//BADGES///////////////////////////////////////////////////
.directive('matBadges',function(){
    return{
        scope:{
            element:'='
        },
        controller:function($scope){},
        restrict:'EA',
        template:'<div class="collection mat-badge "><a href="{{element.link}}" class="collection-item"><ng-transclude></ng-transclude></a></div>',
        replace:false,
        transclude:true
    };
})
//BUTTONS///////////////////////////////////////////////////
.directive('matRaisedButton',function(){
    return{
        scope:{
            element:'='
        },
        controller:function($scope){},
        restrict:'EA',
        template:'<a class=" mat-raised-button waves-effect waves-light btn"><ng-transclude></ng-transclude> {{element.icon}}</a>',
        replace:false,
        transclude:true
    };
})

.directive('matFabButton',function(){
    return{
        scope:{
            element:'='
        },
        controller:function($scope){},
        restrict:'EA',
        template:'<a class=" mat-fab-button btn-floating btn-large waves-effect waves-light "><ng-transclude></ng-transclude> {{element.icon}}</a>',
        replace:false,
        transclude:true
    };
})
.directive('matFlatButton',function(){
    return{
        scope:{
            element:'='
        },
        controller:function($scope){},
        restrict:'EA',
        template:'<a class=" mat-flat-button waves-effect btn-flat "><ng-transclude></ng-transclude> {{element.icon}}</a>',
        replace:false,
        transclude:true
    };
})
.directive('matSendButton',function(){
    return{
        scope:{
            element:'='
        },
        controller:function($scope){},
        restrict:'EA',
        template:'<a class=" mat-send-button btn waves-effect waves-light "><ng-transclude></ng-transclude> {{element.icon}}</a>',
        replace:false,
        transclude:true
    };
})
.directive('matGrandeButton',function(){
    return{
        scope:{
            element:'='
        },
        controller:function($scope){},
        restrict:'EA',
        template:'<a class=" mat-grande-button waves-effect waves-light btn-large "><ng-transclude></ng-transclude> {{element.icon}}</a>',
        replace:false,
        transclude:true
    };
})
//CHIPS///////////////////////////////////////////////////
.directive('matChip',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'<div class=" chip mat-"><ng-transclude></ng-transclude></div>',
        replace:false,
        transclude:true
    };
})
.directive('matImgChip',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'' +
        ' <div class="chip mat-">\n' +
        '     <img src="{{element.src}}" alt="{{element.alt}}">\n' +
        '     <ng-transclude></ng-transclude>' +
        ' </div>',
        replace:false,
        transclude:true
    };
})
//COLLECTIONS///////////////////////////////////////////////////
.directive('matCollectionItem',function(){
    return{
        scope:{
            element:'='
        },
        controller:function($scope){},
        restrict:'EA',
        template:'<li class="collection-item mat-"><ng-transclude></ng-transclude></li>',
        replace:false,
        transclude:true
    };
})
.directive('matCollectionLink',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'<a href="{{element.url}}" class="collection-item"><ng-transclude></ng-transclude></a>',
        replace:false,
        transclude:true
    };
})
.directive('matCollectionHeader',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'<li class="collection-header"><h4><ng-transclude></ng-transclude></h4></li>',
        replace:false,
        transclude:true
    };
})
//ICONS///////////////////////////////////////////////////
.directive('matIcons',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'<i class="material-icons"><ng-transclude"></ng-transclude></i>',
        replace:false,
        transclude:true
    };
})
//INPUT///////////////////////////////////////////////////
.directive('matFormInput',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        template:'' +
        '<div class="input-field">\n' +
        '   <input placeholder="{{element.placeholder}}" id="{{element.id}}" type="{{element.type}}" class="validate" ng-model="content"\n' +
        '   <label for="{{element.label}}">{{element.label}}</label>\n' +
        '</div>',
        replace:false,
        transclude:true
    };
})
//FORMCONTAINER///////////////////////////////////////////////////
.directive('matFormContainer',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:' ' +
        '<div class="row">\n' +
        '    <form class="col s12">\n' +
        '       <ng-transclude></ng-transclude>\n' +
        '    </form>\n' +
        '</div>',
        replace:false,
        transclude:true
    };
})
//DROPDOWN///////////////////////////////////////////////////
.directive('matRoutedDropdownList',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'<li ng-repeat="el in element"><a ui-sref="{{el.url}}">{{el.label}}</a></li>\n',
        replace:false,
        transclude:true
    };
})
.directive('matLinkedDropdownList',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'<li ng-repeat="el in element"><a href="{{el.url}}">{{el.label}}</a></li>\n',
        replace:false,
        transclude:true
    };
})
.directive('matDropdownDivider',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'<li class="divider"></li>\n',
        replace:false,
        transclude:true
    };
})
//IMAGES///////////////////////////////////////////////////
.directive('matImg',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
        },
        controller:function($scope){},
        template:'<img src="{{element.src}}" class="responsive-img " alt="{{element.alt}}">\n',
        replace:false,
        transclude:true
    };
})
.directive('matImgBoxed',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'<img src="{{element.src}}" class="responsive-img materialboxed" alt="{{element.alt}}">\n',
        replace:false,
        transclude:true
    };
})
//PROGRESS BARS////////////////////////////////////////////
.directive('matProgressbar',function(){
    return{
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        restrict:'EA',
        template:'' +
        '  <div class="progress">\n' +
        '      <div class="determinate" style="width: {{content}}%"></div>\n' +
        '  </div>',
        replace:false,
        transclude:true
    };
})
.directive('matProgressbarIdle',function(){
    return{
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        restrict:'EA',
        template:'' +
        '  <div class="progress">\n' +
        '      <div class="indeterminate"></div>\n' +
        '  </div>',
        replace:false,
        transclude:true
    };
})
.directive('matCircleSpinner',function(){
    return{
        scope:{
            element:'=',
        },
        controller:function($scope){},
        restrict:'EA',
        template:'' +
        '  <div class="preloader-wrapper big active">\n' +
        '    <div class="spinner-layer spinner-{{element.textcolor}}-only">\n' +
        '      <div class="circle-clipper left">\n' +
        '        <div class="circle"></div>\n' +
        '      </div><div class="gap-patch">\n' +
        '        <div class="circle"></div>\n' +
        '      </div><div class="circle-clipper right">\n' +
        '        <div class="circle"></div>\n' +
        '      </div>\n' +
        '    </div>\n' +
        '  </div>',
        replace:false,
        transclude:true
    };
})

//COMPOSITE COMPONENTS-----------------------------------------------------------------------------------------------------

//BREADCRUMBS///////////////////////////////////////////////////
.directive('matBreadcrumbs',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        template:'  ' +
        '  <nav>\n' +
        '    <div class="nav-wrapper mat-">\n' +
        '      <div class="col s12">\n' +
        '        <a href="#!" class="breadcrumb" ng-repeat="item in content">{{item}}</a>\n' +
        '      </div>\n' +
        '    </div>\n' +
        '  </nav>',
        replace:false,
        transclude:true
    };
})
//FAB BAR///////////////////////////////////////////////////
.directive('matHorizontalFabBar',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        template:'' +
        '  <div class="fixed-action-btn horizontal" style="position: absolute; right: 24px;">\n' +
        '    <a class="btn-floating btn-large red">\n' +
        '      <i class="large material-icons">mode_edit</i>\n' +
        '    </a>\n' +
        '    <ul>\n' +
        '      <li ng-repeat="item in content" ng-click="item.function()"><a class="btn-floating {{item.bgcolor}}"><i class="material-icons">{{item.icon}}</i></a></li>\n' +
        '    </ul>\n' +
        '  </div>',
        replace:false,
        transclude:true
    };
})
.directive('matHorizontalToggleFabBar',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        template:'' +
        '  <div class="fixed-action-btn horizontal click-to-toggle"  style="position: absolute; right: 24px;">\n' +
        '    <a class="btn-floating btn-large red">\n' +
        '      <i class="large material-icons">menu</i>\n' +
        '    </a>\n' +
        '    <ul>\n' +
        '      <li ng-repeat="item in content" ng-click="item.function()"><a class="btn-floating {{item.bgcolor}}"><i class="material-icons">{{item.icon}}</i></a></li>\n' +
        '    </ul>\n' +
        '  </div>',
        replace:false,
        transclude:true
    };
})
//COLLECTIONS///////////////////////////////////////////////////
.directive('matCollection',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){

        },
        template:'' +
        '<ul class="collection with-header">\n' +
        '    <mat-collection-header>{{element.title}}</mat-collection-header>\n' +
        '    <mat-collection-item ng-repeat="item in content">{{item.label}}</mat-collection-item>\n' +
        '</ul>',

        replace:false,
        transclude:true
    };
})
//COLLECTIONS///////////////////////////////////////////////////
.directive('matCollectionBasic',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){

        },
        template:'' +
        '<ul class="collection with-header">\n' +
        '    <mat-collection-link ng-repeat="item in content" element="item.url">{{item.label}}</mat-collection-link>\n' +
        '</ul>',

        replace:false,
        transclude:true
    };
})
//CARDS///////////////////////////////////////////////////
.directive('matCard',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'      ' +
        ' <div class="row">\n' +
        '   <div class="col s12 m6">\n' +
        '     <div class="card mat-">\n' +
        '       <div class="card-content {{element.textcolor}}-text">\n' +
        '         <span class="card-title">{{element.title}}</span>\n' +
        '         <p>{{element.description}}</p>\n' +
        '         <ng-transclude></ng-transclude> \n' +
        '       </div>\n' +
        '       <div class="card-action">\n' +
        '         <a href="{{element.url}}" class="{{element.linkcolor}}">{{element.label}}</a>\n' +
        '       </div>\n' +
        '     </div>\n' +
        '   </div>\n' +
        ' </div>',
        replace:false,
        transclude:true
    };
})
.directive('matCardImage',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'' +
        ' <div class="row">\n' +
        '   <div class="col s12 m7">\n' +
        '     <div class="card mat-">\n' +
        '       <div class="card-image">\n' +
        '         <img src="{{element.src}}">\n' +
        '         <span class="card-title">{{element.title}}</span>\n' +
        '       </div>\n' +
        '       <div class="card-content">\n' +
        '         <p>{{element.description}}</p>\n' +
        '         <ng-transclude></ng-transclude> \n' +
        '       </div>\n' +
        '       <div class="card-action">\n' +
        '         <a href="{{element.url}}">{{element.label}}</a>\n' +
        '       </div>\n' +
        '     </div>\n' +
        '   </div>\n' +
        ' </div>',
        replace:false,
        transclude:true
    };
})
.directive('matCardImageFab',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'  ' +
        '  <div class="row">\n' +
        '    <div class="col s12 m6">\n' +
        '      <div class="card mat-">\n' +
        '        <div class="card-image">\n' +
        '          <img src="{{element.src}}">\n' +
        '          <span class="card-title">{{element.title}}</span>\n' +
        '          <a class="btn-floating halfway-fab waves-effect waves-light {{element.buttoncolor}}" ng-click="element.function()"><i class="material-icons">{{element.icon}}</i></a>\n' +
        '        </div>\n' +
        '        <div class="card-content">\n' +
        '          <p>{{element.description}}</p>\n' +
        '          <ng-transclude></ng-transclude> \n' +
        '        </div>\n' +
        '      </div>\n' +
        '    </div>\n' +
        '  </div>',
        replace:false,
        transclude:true
    };
})
.directive('matHorizontalCard',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'' +
        '  <div class="col s12 m7">\n' +
        '    <h2 class="header">{{element.title}}</h2>\n' +
        '    <div class="card horizontal mat-">\n' +
        '      <div class="card-image">\n' +
        '        <img src="{{element.src}}" width="400px">\n' +
        '      </div>\n' +
        '      <div class="card-stacked">\n' +
        '        <div class="card-content">\n' +
        '          <p>{{element.description}}</p>\n' +
        '          <ng-transclude></ng-transclude> ' +
        '        </div>\n' +
        '        <div class="card-action">\n' +
        '          <a href="{{element.url}}">{{element.label}}</a>\n' +
        '        </div>\n' +
        '      </div>\n' +
        '    </div>\n' +
        '  </div>',
        replace:false,
        transclude:true
    };
})
.directive('matCardReveal',function(){
    return{
        restrict:'EA',
        scope:{
            element:'='
        },
        controller:function($scope){},
        template:'' +
        '  <div class="card">\n' +
        '    <div class="card-image waves-effect waves-block waves-light mat-">\n' +
        '      <img class="activator" src="{{element.src}}">\n' +
        '    </div>\n' +
        '    <div class="card-content">\n' +
        '      <span class="card-title activator grey-text text-darken-4">{{element.title}}<i class="material-icons right">{{element.icon}}</i></span>\n' +
        '      <p><a href="{{element.url}}">{{element.label}}</a></p>\n' +
        '    </div>\n' +
        '    <div class="card-reveal">\n' +
        '      <span class="card-title grey-text text-darken-4">{{element.title}}<i class="material-icons right">close</i></span>\n' +
        '      <p>{{element.description}}</p>\n' +
        '      <ng-transclude></ng-transclude> ' +
        '    </div>\n' +
        '  </div>',
        replace:false,
        transclude:true
    };
})
//NAVBAR///////////////////////////////////////////////////
.directive('matNavbarRight',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        template:'' +
        '  <nav>\n' +
        '    <div class="nav-wrapper mat-">\n' +
        '      <a href="#" class="brand-logo">{{element.label}}</a>\n' +
        '      <ul id="nav-mobile" class="right hide-on-med-and-down">\n' +
        '        <li ng-repeat="el in content.data" ui-sref-active="active"><a ui-sref="{{el.url}}">{{el.label}}</a></li>\n' +
        '      </ul>\n' +
        '    </div>\n' +
        '  </nav>',
        replace:false,
        transclude:true
    };
})
.directive('matNavbarLeft',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        template:'' +
        '  <nav>\n' +
        '    <div class="nav-wrapper mat-">\n' +
        '      <a href="#" class="brand-logo right">{{element.label}}</a>\n' +
        '      <ul id="nav-mobile" class="left hide-on-med-and-down">\n' +
        '        <li ng-repeat="el in content.data" ui-sref-active="active"><a ui-sref="{{el.url}}">{{el.label}}</a></li>\n' +
        '        <ng-transclude></ng-transclude>' +
        '      </ul>\n' +
        '    </div>\n' +
        '  </nav>',
        replace:false,
        transclude:true

    };
})
.directive('matNavbarCenter',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        template:'' +
        '  <nav>\n' +
        '    <div class="nav-wrapper mat-">\n' +
        '      <a href="#" class="brand-logo center">{{element.label}}</a>\n' +
        '      <ul id="nav-mobile" class="left hide-on-med-and-down">\n' +
        '        <li ng-repeat="el in content.data" ui-sref-active="active"><a ui-sref="{{el.url}}">{{el.label}}</a></li>\n' +
        '        <ng-transclude></ng-transclude>' +
        '      </ul>\n' +
        '    </div>\n' +
        '  </nav>',
        replace:false,
        transclude:true
    };
})
.directive('matNavbarDropdown',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        template:'' +
        '<ul id="dropdownmenu" class="dropdown-content">\n' +
        '  <ng-transclude></ng-transclude> \n' +
        '</ul>\n' +
        '<nav>\n' +
        '  <div class="nav-wrapper mat-">\n' +
        '    <a href="#!" class="brand-logo">{{element.label}}</a>\n' +
        '    <ul class="right hide-on-med-and-down">\n' +
        '      <li  ng-repeat="el in content.data" ui-sref-active="active"><a ui-sref="{{el.url}}">{{el.label}}</a></li>\n' +
        '      <!-- Dropdown Trigger -->\n' +
        '      <li><a class="dropdown-button" href="#!" data-activates="dropdownmenu">Dropdown<i class="material-icons right">arrow_drop_down</i></a></li>\n' +
        '    </ul>\n' +
        '  </div>\n' +
        '</nav>',
        replace:false,
        transclude:true
    };
})
.directive('matSearchbar',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
        },
        controller:function($scope){},
        template:'' +
        '  <nav>\n' +
        '    <div class="nav-wrapper mat-">\n' +
        '      <form>\n' +
        '        <div class="input-field">\n' +
        '          <input id="search" type="search" required>\n' +
        '          <label class="label-icon" for="search"><i class="material-icons">search</i></label>\n' +
        '          <i class="material-icons">close</i>\n' +
        '        </div>\n' +
        '      </form>\n' +
        '    </div>\n' +
        '  </nav>',
        replace:false,
        transclude:true
    };
})
//PAGINATION///////////////////////////////////////////////////
.directive('matPagination',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        template:'' +
        '  <ul class="pagination">\n' +
        '    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>\n' +
        '    <li class="waves-effect" ng-repeat="item in content"><a ui-sref-active="active" ui-sref="{{item.url}}">{{$index+1}}</a></li>\n' +
        '    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>\n' +
        '  </ul>',
        replace:false,
        transclude:true
    };
})

//FOOTER///////////////////////////////////////////////////////

.directive('matFooter',function(){
    return{
        restrict:'EA',
        scope:{
            element:'=',
            content:'='
        },
        controller:function($scope){},
        template:'' +
        ' <footer class="page-footer">\n' +
        '   <div class="container">\n' +
        '     <div class="row">\n' +
        '       <div class="col l6 s12">\n' +
        '         <h5 class="white-text">Footer Content</h5>\n' +
        '         <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>\n' +
        '       </div>\n' +
        '       <div class="col l4 offset-l2 s12">\n' +
        '         <h5 class="white-text">Links</h5>\n' +
        '         <ul>\n' +
        '           <li ng-repeat="item in content"><a class="grey-text text-lighten-3" href="{{item.url}}">{{item.label}}</a></li>\n' +
        '         </ul>\n' +
        '       </div>\n' +
        '     </div>\n' +
        '   </div>\n' +
        '   <div class="footer-copyright">\n' +
        '     <div class="container">\n' +
        '     © 2014 Copyright Text\n' +
        '     <a class="grey-text text-lighten-4 right" href="#!">More Links</a>\n' +
        '     </div>\n' +
        '   </div>\n' +
        ' </f',
        replace:false,
        transclude:true
    };
})
//// ATTRIBUTE DIRECTIVES /////////////////////////////////////////
//     .directive('draggable', function($document) {
//     return function(scope, element, attr) {
//         var startX = 0, startY = 0, x = 0, y = 0;
//         element.css({
//             position: 'relative',
//             cursor: 'pointer',
//             display: 'block',
//         });
//         element.on('mousedown', function(event) {
//             // Prevent default dragging of selected content
//             event.preventDefault();
//             startX = event.screenX - x;
//             startY = event.screenY - y;
//             $document.on('mousemove', mousemove);
//             $document.on('mouseup', mouseup);
//         });
//
//         function mousemove(event) {
//             y = event.screenY - startY;
//             x = event.screenX - startX;
//             element.css({
//                 top: y + 'px',
//                 left:  x + 'px'
//             });
//         }
//
//         function mouseup() {
//             $document.off('mousemove', mousemove);
//             $document.off('mouseup', mouseup);
//         }
//     };
//     // interesting but need tweaks for z-index
// })
;//end directives