var moduleService = angular.module('app.services',[]);

moduleService.factory('socket', function ($rootScope) {

    var socket = io.connect();
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    };
});

moduleService.factory('serviceClientApi',function(socket){
    var base_template = function(arg){

        var template = {
            id:arg.id,
            type:arg.type,
            label:arg.label,
            title:arg.title,
            description:arg.description,
            icon:arg.icon,
            buttonicon:arg.buttonicon,
            route:arg.route,
            link:arg.link,
            url:arg.url,
            src:arg.src,
            alt:arg.alt,
            size:arg.size,
            placeholder:arg.placeholder,
            content:arg.content,
            data:arg.data,
            value:arg.value,
            function:arg.function
        };
        return template;
    };
    return {
        getServerPackage:function(){
            socket.emit('getServerPackage', {})//eventually add which package is required
        },
        newTemplateInstance:function(arg){
            var template_instance = base_template(arg);
            return template_instance;
        }
    }

});

