angular.module('app.view4',[])
    .controller('view4Ctrl',function($scope,serviceClientApi,socket){

        //request the data from the server then cache it
        serviceClientApi.getServerPackage();
        $scope.server_package = {};
        $scope.default_templates = {};
        $scope.template_list =[];
        $scope.form_template = {};
        $scope.form_model = {};

        $scope.selectedUiItem = 21;

        $scope.page_content = [];

        $scope.formSetup = function(index){
            $scope.form_template = buildInputTemplate(getTemplateList(getTemplate($scope.default_templates,$scope.template_list[index])));
            $scope.form_model = buildInputModel(getTemplateList(getTemplate($scope.default_templates,$scope.template_list[index])));
        };

        $scope.formUiSelect = function(arg){
            $scope.selectedUiItem = arg;
            $scope.formSetup($scope.selectedUiItem);
        };

        $scope.sendFormData = function(arg){
            var data_list = Object.keys(arg);
            for(var i = 0;i <= data_list.length;i++){
                if(data_list[i] !== undefined && data_list[i] !== ""){
                    if($scope.form_template[data_list[i]].value !== undefined && $scope.form_template[data_list[i]].value !==""){
                        console.log($scope.form_model[data_list[i]]);
                        // console.log($scope.form_template[data_list[i]].value);
                        $scope.form_model[data_list[i]] = $scope.form_template[data_list[i]].value;
                        //reset data for next entry
                    }
                }
            }
            $scope.page_content.push($scope.form_model);
            $scope.formSetup($scope.selectedUiItem);
            // formReset($scope.selectedUiItem);
            // console.log($scope.form_model);
            // console.log($scope.form_template);
        };

        //listen to the server response from the serverpackage request
        socket.on('postServerPackage', function (pack) {
            $scope.server_package = pack.data;
            $scope.default_templates = pack.template;
            $scope.template_list = getTemplateList($scope.default_templates);
            $scope.formSetup($scope.selectedUiItem);
            ////traces
            // console.log('data recieved : ',$scope.server_package);
            // console.log('list : ',$scope.template_list);
            // console.log('template : ',$scope.form_template);
            // console.log('model : ',$scope.form_model);
        });

        function getTemplateList(arg){
            return Object.keys(arg);
        }

        function getTemplate(list,template){
             return list[template];
        }

        function buildInputTemplate(arg_array){
            var input_template ={};
            for(var i = 0;i <= arg_array.length;i++){
                if(arg_array[i] !== undefined && arg_array[i] !== ""){
                    input_template[arg_array[i]] = {
                        placeholder:arg_array[i],
                        id:arg_array[i],
                        label:arg_array[i],
                        type:'text',
                        classes:'col s6',
                        value:''
                    }
                }
            }
            return input_template;
        }

        function buildInputModel(arg_array){
            var input_model ={};
            for(var i = 0;i <= arg_array.length;i++){
                if(arg_array[i] !== undefined && arg_array[i] !== ""){
                    input_model[arg_array[i]] = '';
                }
            }
            return input_model;
        }

        //JQUERY GO HERE
        $(document).ready(function(){

        });

    });
