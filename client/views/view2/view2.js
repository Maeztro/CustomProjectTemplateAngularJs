angular.module('app.view2',[])
    .controller('view2Ctrl',function($scope,socket,serviceClientApi){

        //request the data from the server then cache it
        serviceClientApi.getServerPackage();
        $scope.server_package = {};
        $scope.default_templates = {};

        //listen to the server response from the serverpackage request
        socket.on('postServerPackage', function (pack) {
            $scope.server_package = pack.data;
            $scope.default_templates = pack.template;
            //console.log('data recieved : ',$scope.server_package);
        });

        $scope.list_form_data = [];

        $scope.form_cache = {
            firstname: '',
            lastname: '',
            email: '',
            username: '',
            password: ''
        };

        $scope.sendFormData = function (){
            $scope.list_form_data.push({
                firstname:  $scope.form_cache.firstname,
                lastname: $scope.form_cache.lastname,
                email: $scope.form_cache.email,
                username: $scope.form_cache.username,
                password: $scope.form_cache.password
            });
            // alert('data sent!');
            console.log($scope.list_form_data);
            resetFormCache();
        };

        function resetFormCache(){
            $scope.form_cache.firstname = '';
            $scope.form_cache.lastname = '';
            $scope.form_cache.email = '';
            $scope.form_cache.username = '';
            $scope.form_cache.password = '';
        }

        //JQUERY GO HERE
        $( document ).ready(function(){
            $(".dropdown-button").dropdown();
        });

    });