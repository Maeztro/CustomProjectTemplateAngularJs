angular.module('app.view1',[])
    .controller('view1Ctrl',function($scope,socket,serviceClientApi){

        //request the data from the server then cache it
        serviceClientApi.getServerPackage();
        $scope.server_package = {};
        $scope.default_templates = {};

        //listen to the server response from the serverpackage request
        socket.on('postServerPackage', function (pack) {
            $scope.server_package = pack.data;
            $scope.default_templates = pack.template;
            // console.log('data recieved : ',$scope.server_package , $scope.default_templates);
        });

        $scope.loading = {progress:60};

        //JQUERY GO HERE
        $(document).ready(function(){
            $('.materialboxed').materialbox();
        });
    });
