angular.module('app.view3',[])
    .controller('view3Ctrl',function($scope,serviceClientApi,socket){

        //request the data from the server then cache it
        serviceClientApi.getServerPackage();
        $scope.server_package = {};
        $scope.default_templates = {};

        //listen to the server response from the serverpackage request
        socket.on('postServerPackage', function (pack) {
            $scope.server_package = pack.data;
            $scope.default_templates = pack.template;
            //console.log('data recieved : ',$scope.server_package);
        });

        //JQUERY GO HERE
        $(document).ready(function(){
            $('.carousel').carousel();
            $('.carousel.carousel-slider').carousel({fullWidth: true});
        });


    });