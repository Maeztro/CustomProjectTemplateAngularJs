
var express = require ('express');
var app = express();
var serv = require('http').Server(app);

app.use(express.static("../client"));
app.use('/client', express.static(__dirname + '/client'));

serv.listen(2000);
console.log('server started');

//////////////////DATA TEMPLATES////////////////////////////////////////////////////////////////////////////////////////

var element = function(arg){

    var template = {
        id:arg.id,
        type:arg.type,
        label:arg.label,
        title:arg.title,
        description:arg.description,
        icon:arg.icon,
        buttonicon:arg.buttonicon,
        route:arg.route,
        link:arg.link,
        url:arg.url,
        src:arg.src,
        alt:arg.alt,
        size:arg.size,
        placeholder:arg.placeholder,
        content:arg.content,
        data:arg.data,
        value:arg.value,
        function:arg.function
    };
    return template;
};

//////////////////DEFAULT TEMPLATES////////////////////////////////////////////////////////////////////////////////////////

var default_badge = element({link:'view1',url:'https://unsplash.com'});

var default_raised_button = element({label:'Button',function:function(){console.log(this.bgcolor)}});
var default_raised_icon_button = element({label:'Button',function:function(){console.log(this.bgcolor)},buttonicon:'android'});
var default_fab_button = element({label:'Button',function:function(){console.log(this.bgcolor)},buttonicon:'menu'});
var default_flat_button = element({label:'Button',function:function(){console.log(this.bgcolor)}});
var default_send_button = element({label:'Button',function:function(){console.log(this.bgcolor)},buttonicon:'send',type:'submit'});
var default_grande_button = element({label:'Button',function:function(){console.log(this.bgcolor)}});

var default_chip = element({src:'http://placekitten.com/48/48',alt:'kitten',label:'a cute kitten'});

var default_collection = element({title:'Collection header'});
var default_collection_link = element({label:'Link',url:'http://materializecss.com'});
var default_collection_header = element({title:'Collection header'});

var default_icon = element({icon:'android'});

var default_text_input = element({placeholder:'first-name',id:'firstname',type:'text',label:'Text'});

var default_dropdown_container = element({id:'dropdown1'});
var default_routed_dropdown_link = element({route:'view1',label:'view1'});
var default_linked_dropdown_link = element({url:'http://materializecss.com',label:'materialize'});
var default_dropdown_button = element({icon:'arrow_drop_down',id:'dropdown1'});

var default_img = element({src:'https://images.unsplash.com/photo-1518607923879-19117169a071?ixlib=rb-0.3.5&s=bfd5aa32d766f771b54c6d499293784c&auto=format&fit=crop&w=1950&q=80',alt:'New York by Chris Barbalis'});

var default_breadcrumbs = element({});

var default_fab_bar = element({buttonicon:'menu'});

var default_navbar = element({label:'Logo'});
var default_searchbar = element({label:'Search',icon:'search'});

var default_pagination = element({});

var default_progress = element({});

var default_footer = element({title:'Footer',description:'This is a footer where you can add extra links at the end of your page',link:'see also'});

var default_card_basic = element({
    title:'Example card',
    description:'This is an example card, cards are used to display small paragraphs of information with an image,links and buttons',
    url:'https://unsplash.com',
    label:'New York, by Chris Barbalis'
});
var default_card_image = element({
    title:'Example card',
    description:'This is an example card, cards are used to display small paragraphs of information with an image,links and buttons',
    url:'https://unsplash.com',
    label:'New York, by Chris Barbalis',
    src:'https://images.unsplash.com/photo-1518607923879-19117169a071?ixlib=rb-0.3.5&s=bfd5aa32d766f771b54c6d499293784c&auto=format&fit=crop&w=1950&q=80',
});
var default_card_image_fab = element({
    title:'Example card',
    description:'This is an example card, cards are used to display small paragraphs of information with an image,links and buttons',
    url:'https://unsplash.com',
    label:'New York, by Chris Barbalis',
    src:'https://images.unsplash.com/photo-1518607923879-19117169a071?ixlib=rb-0.3.5&s=bfd5aa32d766f771b54c6d499293784c&auto=format&fit=crop&w=1950&q=80',
    icon:'add',
    function:function(){alert(this.textcolor);}
});
var default_card_horizontal = element({
    title:'Example card',
    description:'This is an example card, cards are used to display small paragraphs of information with an image,links and buttons',
    url:'https://unsplash.com',
    label:'New York, by Chris Barbalis',
    src:'https://images.unsplash.com/photo-1518607923879-19117169a071?ixlib=rb-0.3.5&s=bfd5aa32d766f771b54c6d499293784c&auto=format&fit=crop&w=1950&q=80',
});
var default_card_reveal = element({
    title:'Example card',
    description:'This is an example card, cards are used to display small paragraphs of information with an image,links and buttons',
    url:'https://unsplash.com',
    label:'New York, by Chris Barbalis',
    src:'https://images.unsplash.com/photo-1518607923879-19117169a071?ixlib=rb-0.3.5&s=bfd5aa32d766f771b54c6d499293784c&auto=format&fit=crop&w=1950&q=80',
    icon:'more_vert',
    function:function(){alert(this.textcolor);}
});
var template_package = {
    badge:default_badge,
    raised_button:default_raised_button,
    raised_icon_button:default_raised_icon_button,
    fab_button:default_fab_button,
    flat_button:default_flat_button,
    send_button:default_send_button,
    grande_button:default_grande_button,
    chip:default_chip,
    collection:default_collection,
    collection_link:default_collection_link,
    collection_header:default_collection_header,
    icon:default_icon,
    text_input:default_text_input,
    dropdown_container:default_dropdown_container,
    routed_dropdown_link:default_routed_dropdown_link,
    linked_dropdown_link:default_linked_dropdown_link,
    dropdown_button:default_dropdown_button,
    img:default_img,
    breadcrumbs:default_breadcrumbs,
    fab_bar:default_fab_bar,
    cardbasic:default_card_basic,
    cardimage:default_card_image,
    cardimagefab:default_card_image_fab,
    cardhorizontal:default_card_horizontal,
    cardreveal:default_card_reveal,
    navbar:default_navbar,
    searchbar:default_searchbar,
    pagination:default_pagination,
    progress:default_progress,
    footer:default_footer
};


//// MOCK DATA /////////////////////////////////////////////////////////////////////////////////////////////////////////

var default_list = ['One Shot at glory','Two: In the crossfires overhead','Three: Fate stands before me ','Four: Words have all been said'];
var default_enum = ['One','Two','Three','Four'];

var color_list = [
    'red',
    'pink',
    'purple',
    'deep-purple',
    'indigo',
    'blue',
    'light-blue',
    'cyan',
    'teal',
    'green',
    'light-green',
    'lime',
    'yellow',
    'amber',
    'orange',
    'deep-orange',
    'brown',
    'grey',
    'blue-grey',
    'white',
    'black',
    'transparent'
];

var icon_list = ['add_circle',
    'access_time',
    'add_a_photo',
    'add_location',
    'account_circle',
    'arrow_drop_down_circle',
    'beenhere',
    'call',
    'check_circle',
    'computer',
    'directions_bike',
    'gps_fixed',
    'headset',
    'notifications'
];

var collection_data = [
    {label:'New York, by Chris Barbalis',url:'https://images.unsplash.com/photo-1518607923879-19117169a071?ixlib=rb-0.3.5&s=bfd5aa32d766f771b54c6d499293784c&auto=format&fit=crop&w=1950&q=80'},
    {label:'Tokyo, by Andre Benz',url:'https://images.unsplash.com/photo-1493514789931-586cb221d7a7?ixlib=rb-0.3.5&s=01de8c2ffe7a269bba20678ce2726957&auto=format&fit=crop&w=1951&q=80'},
    {label:'Hong Kong, by Kristo Vedenoja',url:'https://images.unsplash.com/photo-1516556294316-055e8753eb69?ixlib=rb-0.3.5&s=73267476312dc3a7639b1ce40d63daf9&auto=format&fit=crop&w=2134&q=80'},
    {label:'Toronto, by Brxxto',url:'https://images.unsplash.com/photo-1514613453913-ec5da0db2faa?ixlib=rb-0.3.5&s=21562790e4251ea6bfd0d192e97fe6bf&auto=format&fit=crop&w=1350&q=80'}
];

var fab_data = [
    {
        bgcolor:'red',
        icon:'insert_chart',
        function:function(){alert(this.bgcolor);}
    },
    {
        classes:'btn-floating',
        bgcolor:'yellow',
        icon:'format_quote',
        function:function(){alert(this.bgcolor);}
    },
    {
        bgcolor:'green',
        icon:'publish',
        function:function(){alert(this.bgcolor);}
    },
    {
        bgcolor:'blue',
        icon:'attach_file',
        function:function(){alert(this.bgcolor);}
    }
];

var form_data = {
    firstname:{
        placeholder:'first name',
        id:'first_name',
        label:'First name:',
        type:'text',
        classes:'col s6',
        data:''
    },
    lastname:{
        placeholder:'last name',
        id:'last_name',
        label:'Last name:',
        type:'text',
        classes:'col s6',
        data:''
    },
    email:{
        placeholder:'e-mail',
        id:'email',
        label:'E-mail:',
        type:'email',
        classes:'col s12',
        data:''
    },
    username:{
        placeholder:'username',
        id:'username',
        label:'Username:',
        type:'text',
        classes:'col s12',
        data:''
    },
    password:{
        placeholder:'password',
        id:'password',
        label:'Password:',
        type:'password',
        classes:'col s12',
        data:''
    }
};

var navbar_data = {
    label:'Logo',
    data:[
        {label:'page 1',url:'view1'},
        {label:'page 2',url:'view2'},
        {label:'page 3',url:'view3'},
        {label:'page 4',url:'view4'},
    ]
};

var dropdown_data = [
        {label:'page 1',url:'view1'},
        {label:'page 2',url:'view2'},
        {label:'page 3',url:'view3'},
        {label:'page 4',url:'view4'},
    ];

var page_data =[
    {url:'view1'},
    {url:'view2'},
    {url:'view3'},
    {url:'view4'}
];

var data_package = {
    list:default_list,
    enum:default_enum,
    colors:color_list,
    icons:icon_list,
    collection:collection_data,
    fab:fab_data,
    form:form_data,
    nav:navbar_data,
    dropdown:dropdown_data,
    pages:page_data
};

var io = require('socket.io')(serv,{});
io.sockets.on('connection', function(socket) {

    socket.on('getServerPackage', function(){
        socket.emit('postServerPackage', {
            data: data_package,
            template:template_package
        });
    });

    socket.on('ping!', function(){
        socket.emit('pong!', {
            package: ''
        });
    });

});

//to launch the server, simply open a terminal to this file root folder and enter 'node app.js'
//to connect to the server in localhost , go to localhost:2000 ( or whatever server port you entered in "serv.listen();")
//enjoy!
